# ABM firm simulator

A model of a company with production lines, resource puchasing and sales. Intend to use it to generate databases which I'll use for SQL problems on fetching data.

## Workflow

1. Prepare task (responsible: Danila).
    - create a feature branch (dev_*featurename*)
    - sketch the classes and key functions
    - write tests
    - write TODO's
    - add detailed comments where necessary
    - set the person responsible for the task
2. Do the task (responsible: Danila / Kate)
    - write the code
    - make sure the code complies with the TODO's and comments
    - remove all TODO's that have been done
    - add docstrings (default for classes, and functions that will be
      called from the simulation code)
    - review the comments: remove the obsolete ones, add some if necessary
    - create a merge request
3. Do the code review (responsible: Danila)
    - check that all tests are passed
    - check style
    - check TODO's and comments
    - if everything is ok, merge, otherwise return to stage 2

Checklist before merge request:

- the code passes the tests
- all complete TODO's are deleted
- docstrings are prepared for each class and the most important functions
- all non-trivial actions are commented or described in docstrings
- all outdated comments are deleted
- no scratch code is left
